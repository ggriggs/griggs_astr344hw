print"Georgia Griggs"
print"Project 1: Oh, Polio"
print "AST 344"

##
import numpy
import random
import csv
import matplotlib.pyplot as plt


#defining my over-arching function
#the idea is that as the hours go on there is a chance that the humans come in contact with eachother 
#and the results are different for whether or not the clean population is vaccinated 
 
filename = random.randint(1000,10000)

def spread(clean_pop, polio_pop):
    day = 1
    hour= range(1,25) 
  
    while clean_pop > 0 and polio_pop > 0:
        plt.ion()  #like animation
        for t in hour:
            if t == 24:
                day = day + 1
                print("Today is day " + str(day) + " since the infection started.")
            if clean_pop < 1:
                print("Everyone is sick")
                break
            if polio_pop < 1:
                print("Nobody was sick to start with")
                break
            else:
                if t <= 24 or t > 0:
                    print("day: " + str(day) + " hour:" + str(t))
                    c = contact(clean_pop , polio_pop)
                    clean_pop  = c[0]
                    v = (t,polio_pop,clean_pop )
                    polio_pop = v[0]
                    print("Number of clean folk: " + str(clean_pop ))
                    print("Number of people with polio: " + str(polio_pop))
              #csv syntax  
                plotting(t,clean_pop ,polio_pop)
                with open(str(filename)+'.csv', 'a') as csvfile:
                    spreadwriter = csv.writer(csvfile)
                    spreadwriter.writerow([day, t, clean_pop , polio_pop])
    plt.show()

              
def contact(clean_pop, polio_pop):
    if clean_pop  < 0 or polio_pop < 0:
        return
    p_range = range(0,polio_pop)
    
    for p in p_range: #polio range
        sick_chance = random.randint(1,100) + 70 #you can change the sick chance to make it more or less likley that someone will get sick when they come in contact 
        health_chance = random.randint(1,100) + 60
        
            
        if sick_chance > health_chance:
            polio_pop += 1
            clean_pop  -= 1
    #now saying that the person that the sick person has a chance of being vaccinated 
    #and that chance can be altered
        elif health_chance > sick_chance:
            #check to see if this is a vaccinated encounter vaccination encounter
            vaccination_chance = random.randint(1,100) + 1000
            if vaccination_chance < 80:
                polio_pop += 0 
            elif vaccination_chance >= 80:
                polio_pop += 1
                             
    return clean_pop , polio_pop

def plotting(day,clean_pop ,polio_pop):
 
    plt.plot([day], [polio_pop],'rs',linewidth=3)
    plt.title('Polio Spread')
    plt.ylabel('Population')
    plt.plot([day],[clean_pop],'gs')
    plt.xlabel('Time')
    plt.draw()
    plt.pause(.1)    
    
    
#this is where we define how many humans are involved in oyut function 
spread(100,1)


